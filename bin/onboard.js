#!/usr/bin/env node

'use strict'

let HttpServer = require('../index').HttpServer
,   demon = require('demon')
,   util = require('youtil')
,   args = require('parseargv')(process.argv)
util.usePrototypeExtensions()
  /***
   *  --port:<port#>
  ***/
if( args.remove('foreground') )
  new HttpServer(5555, args) 
else if( ~args.indexOf('kill') ) 
  demon.kill()
else // This will end up creating the server in a child process.
  demon.start(`${__dirname}/onboard.js`, args.concat('foreground'))
