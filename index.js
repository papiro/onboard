'use strict'

let http        = require('http')
,   url         = require('url')
,	  querystring = require('querystring')
,	  fs          = require('fs')
,	  path        = require('path')

let
  APP = process.cwd()
  , 
  index = 'index.html'
  ,
  noop = () => {}

exports.HttpServer = class HttpServer {
  constructor(port, args, onError, onRequest, onListen) {
    var port = port || 80
    ,   onError = onError || noop
    ,   onRequest = onRequest || noop
    ,   onListen = onListen || noop
    this.server = http.createServer()

    Object.assign( this, { onError, onRequest, onListen  } )
    this.bindEvents()

    this.server.listen(port)
  }

  bindEvents() {
    this.server.on('error', this.onError)

    this.server.on('listening', this.onListen)

    this.server.on('request', (request, response) => {
      this.onRequest(request, response)

      let 
        // Resource extension (ie '.html', '.js', '' for a directory)
        resourceType = path.extname(request.url).replace('.', '')
      ,
        serveFile = () => {
          fs.readFile(`${APP}${request.url}`, (err, content) => {
            // Resource not found
            if(err) return notFound()
            response.writeHead(200, {
              // text-based MIME types supported
              'Content-Type': `text/${resourceType}`,
              // Caching disabled for development
              'Expires': Date()
            })
            response.write(content)
            response.end()  
          })
        }
      ,
        serveDirectory = () => {
          fs.readFile(`${APP}${request.url}${index}`, (err, content) => {
            // If there is no index.html at this directory, serve the root index.html
            if(err) return reset()
            response.writeHead(200, {
              'Content-Type': 'text/html',
              // Caching disabled for development
              'Expires': Date()
            })
            response.write(content)
            response.end()
          })
        }
      ,
        reset = () => {
          response.writeHead(302, {
            'Location': '/index.html'
          })
          response.end()
        }
      ,
        notFound = () => {
          response.writeHead(404)
          response.end()
        }

      // If a directory is being requested, serve the index.html at that directory 
      resourceType === '' ? serveDirectory() : serveFile()
    })
  }

  close(callback) {
    this.server.close(callback || noop)
  }

}
